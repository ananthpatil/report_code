package reportformation;


import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import au.com.bytecode.opencsv.CSVReader;

public class PerformanceVersion_One {

	/**The method to generate the sla brached report
	 * It returns the void
	 * and it accepts three parameters
	 * @param path
	 * @param fiVal
	 * @param brandName
	 */	
public void slaBreachedReport(String path,Map<String,List<String[]>> fiVal,String brandName)
{
try{ 
   int totCt=fiVal.size(),incr=0;
   FileWriter writer=new FileWriter(new File(path));
   writer.write("<html><head></head><body style=\"margin: 0 auto;\"><div style=\"display: block; height: 10%; position: fixed; width: 100%; top: 0px; background: none repeat scroll 0% 0% rgb(194, 194, 195);\"><h1 align=\"center\"><u>SLA Breached Transactions</u></h1></div><div style=\"margin-left:60px;margin-right:20px;margin-top: 100px;\">");
			 //writer.write("<div style=\"height:300px; overflow:auto;\">");
   Iterator<String> filIterator=fiVal.keySet().iterator();
   while(filIterator.hasNext())
  {   
	String _keyName = filIterator.next();
	List<String[]> retVal=fiVal.get(_keyName);
	writer.write("<table border=\"3\" width=\"1300\">");
	writer.write("<tr><th width=\"120\">Date&Time</th><th width=\"940\">BrandName_"+_keyName+"</th><th width=\"120\">SLA</th><th width=\"120\">90%Line</th></tr>");
	for(int k=0;k<retVal.size();k++)
	{
	  String[] arr=retVal.get(k);
	  writer.write("<tr><td></td><td>"+arr[0]+"</td><td >"+arr[2]+"</td><td >"+arr[1]+"</td></tr>");
    }
	if(totCt==1||incr==totCt-1){
	   writer.write("</table></div>");
	}else{
	   writer.write("</table><br/><br/>");
	}
	   incr++;
   }
	   writer.write("</div></body></html>");
	   writer.close();
}catch (IOException e){
	e.printStackTrace();
  }
}


/**The method to generate the error report
 * It returns the void
 * It accepts the three parameter
 * @param path
 * @param fiVal
 * @param brandName
 */
public void perfErrorReport(String path,Map<String,List<String[]>> fiVal,String brandName)
{
try{ 
  int totCt=fiVal.size(),incr=0;
  FileWriter writer=new FileWriter(new File(path));
  writer.write("<html><head></head><body style=\"margin: 0 auto;\"><div style=\"display: block; height: 10%; position: fixed; width: 100%; top: 0px; background: none repeat scroll 0% 0% rgb(194, 194, 195);\"><h1 align=\"center\"><u>ErrorReport</u></h1></div><div style=\"margin-left: 60px;margin-right: 20px; margin-top: 100px;\">");
  Iterator<String> filIterator=fiVal.keySet().iterator();
  while(filIterator.hasNext())
  {   String _keyName = filIterator.next();
	  List<String[]> retVal=fiVal.get(_keyName);
      writer.write("<table border=\"3\" width=\"1300\">");
	  writer.write("<tr><th width=\"100\">BrandName</th><th width=\"600\">Request</th><th width=\"100\">Error%</th><th width=\"500\">ErrorDetails</th></tr>");
    for(int k=0;k<retVal.size();k++)
	{
	  String[] arr=retVal.get(k);
	  if(k==0)
	  writer.write("<tr><td>"+_keyName+"</td><td>"+arr[0]+"</td><td >"+arr[1]+"</td><td >"+arr[2]+"</td></tr>");
	  else
	  writer.write("<tr><td></td><td>"+arr[0]+"</td><td >"+arr[1]+"</td><td >"+arr[2]+"</td></tr>");
    }
	if(totCt==1||incr==totCt-1){
	  writer.write("</table></div>");
	}else{
	  writer.write("</table><br/><br/>");
	}
		     incr++;
  }
	writer.write("</div></body></html>");
	writer.close();
}catch (IOException e){
			e.printStackTrace();
  }
}


/**The following method used to sort the list
 * It accepts one parameter
 * It returns the sorrted list
 * @param ltToSort
 */
public List<String[]> sortingTheList(List<String[]> ltToSort)
{
     List<String[]> srtdList = new ArrayList<String[]>();
for(int i = 0; i < ltToSort.size(); i++)
{
     String[] _arr1=ltToSort.get(i);
  for(int j = i+1; j < ltToSort.size(); j++)
  {
	 String[] _arr2=ltToSort.get(j);
	if(Float.parseFloat(_arr1[1]) <= Float.parseFloat(_arr2[1])){
	 String _temp,_temp1,_temp2;
	_temp  = _arr1[1];
	_temp1 = _arr1[0]; 
	_temp2 = _arr1[2];
	_arr1[0] = _arr2[0];
	_arr2[0] = _temp1;
    _arr1[1] = _arr2[1];
    _arr2[1] = _temp;
    _arr1[2] = _arr2[2];
	_arr2[2] = _temp2;
   } 
  }
   srtdList.add(_arr1);
 }
		 return srtdList;
}


/**The method used to return the current date
 * It returns the string
 * @return
 */
public String getCurrentDate()
{
   String curDate = "";
try{
   Date dt=new Date();
   SimpleDateFormat fmt=new SimpleDateFormat("yyyyMMdd-hhmmss");
   curDate=fmt.format(dt);
  }catch(Exception e){
   System.out.println("Execption in while returning the current date:"+e);
 }
  return curDate;
}

/** The method used to delete the HTML files from given folder
  * It accepts one parameter as parent folder path
  */
public void deleteFileFromFolder(String path)
{
try{
	   File resFolder = new File(path);
	   String[] flIndex = resFolder.list();
   for(String _flIndex : flIndex)
   {  if(_flIndex.endsWith(".html")){
	   File fin=new File(resFolder.getPath(), _flIndex);
	   fin.delete();
	   System.out.println("<----Old html reports are deleted--->");
	  } 
   }
  }catch(Exception e){
	   System.out.println("Exception occured while deleting the file from folder: "+e);
  }
}

/**The method used to calculate the days.
 * It returns the latest folder path
 * @param fldDate
 * @return
 */
public String dayCalculation(List<File> fldDate)
{
	Date fdDat,currDat;String _fldPath = "";
try
{   SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
    if(fldDate.size()>=1)
   {
    fdDat=fmt.parse(fldDate.get(0).getName().substring(fldDate.get(0).getName().lastIndexOf("-")+1));
    currDat = new Date();
    int minVal=(int) (currDat.getTime()-fdDat.getTime())/(1000*60*60*24);
    minVal = Math.abs(minVal);
    _fldPath = fldDate.get(0).getAbsolutePath();
   for(int i=1;i<fldDate.size();i++)
   {
     try{
     fdDat=fmt.parse(fldDate.get(i).getName().substring(fldDate.get(i).getName().lastIndexOf("-")+1));
     currDat = new Date();
     int d=(int) (currDat.getTime()-fdDat.getTime())/(1000*60*60*24);
     d = Math.abs(d);
     if(d < minVal)
     {
         minVal = d;
        _fldPath = fldDate.get(i).getAbsolutePath();
     }
    }catch(Exception e){
    	System.out.println("Exception occured while reading the folder: "+e);
    }
  }}
 }catch(Exception e){
    e.printStackTrace();
  }
  return _fldPath;
}

/**The method used filter the latest JTL file from the folder
 * It returns the Set which is having the latest JTL file name
 * @param _fltJtlFile
 * @return
 */
@SuppressWarnings("unused")
public Set<String> flterJtlFiles(List<File> _fltJtlFile)
{
    String frstFlName,secdFlName,fisrtN,secN,firstD,secD,flName = null;
    long small = 0;
	Date scDate,flDate;
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd-HHmmss");
	Set<String> uqFlName = new LinkedHashSet<String>();
	Date currDate = new Date();
	for(int i = 0;i < _fltJtlFile.size();i++)
	{      
		    boolean flag= false;
		    frstFlName = _fltJtlFile.get(i).getName().replace(".jtl", "");
	try{
		if(!frstFlName.equals("Matched")){
		    fisrtN = frstFlName.substring(0,frstFlName.lastIndexOf("-"));
		    firstD =  frstFlName.substring(frstFlName.lastIndexOf("_")+1);
		    flDate=fmt.parse(firstD);
		    
	    for(int j=i+1;j < _fltJtlFile.size();j++)
	   {
	     if(!_fltJtlFile.get(j).getName().equals("Matched")){
		     secdFlName = _fltJtlFile.get(j).getName().replace(".jtl", "");
		   try{
		     secN = secdFlName.substring(0,secdFlName.lastIndexOf("-"));
		     secD = secdFlName.substring(secdFlName.lastIndexOf("_")+1);
		     scDate=fmt.parse(secD);
		    if(fisrtN.trim().equals(secN))
		    {
		    	 flag = true;
		    	 long d =currDate.getTime() - flDate.getTime()/ 1000 % 60;
		    	  d = Math.abs(d);
		    	 long d1 = currDate.getTime() - scDate.getTime()/ 1000 % 60;
		    	 d1 = Math.abs(d1);
		    	 
		    	 if(d < d1){
		    		 small = d; 
		    		 flName = _fltJtlFile.get(i).getAbsolutePath();
		    	 }else{
		    		 small = d1;
		    		 flName = _fltJtlFile.get(j).getAbsolutePath();
		    	}
		    	 _fltJtlFile.set(j, new File("Matched"));
		      }
		    }catch(Exception e){
		    	System.out.println("The exception occured while reading the jtl file:"+e);
		    }
	      }  //Matched end here
         }
	       if(flag==false){
	    	 flName = _fltJtlFile.get(i).getAbsolutePath();
	        }
	        uqFlName.add(flName);
	   }
    }catch(Exception e){
			  e.printStackTrace();
		}
	}
	return uqFlName;
}
	


public static void main(String[] args) 
{
String[] data;
Set<String> _uniqueReqName = null;
List<String> reqNameAndResTm = null;
String jtlFolderPath,resultFolderPath,_brandName,_filDate = null;
List<Integer> resTmList = null;List<String[]> filVal=null,errVal=null;
CSVReader reader1;
	 
jtlFolderPath = "C:/Users/ananthreddy.s/Desktop/TH_WebTestPlan_Results_Tranche/";
//jtlFolderPath = args[0];
resultFolderPath="C:/Users/ananthreddy.s/Desktop/";
//resultFolderPath = args[1];
	 
Map<String,List<Integer>> unqVal = new LinkedHashMap<String,List<Integer>>();
Map<String,List<String[]>> filMap, errorMap;
List<File> _fldName =new ArrayList<File>();
List<File> _jtlFileFlt = new ArrayList<File>();
filMap = new LinkedHashMap<String,List<String[]>>();
errorMap = new LinkedHashMap<String,List<String[]>>();
_brandName = null;
	 
//File _readFolder = new File(jtlFolderPath);
//File[] _listOfFiles=_readFolder.listFiles();
PerformanceVersion_One _perfOb = new PerformanceVersion_One();

File _folder = new File(jtlFolderPath);
if(_folder.exists())
{
   File[] subFolder = _folder.listFiles();
   for(File subFld:subFolder)
  {
    if(subFld.isDirectory() && !subFld.getName().equals("Individual")){   
   try{
      String fdDat = subFld.getName().substring(subFld.getName().lastIndexOf("-")+1);
      if(fdDat.length()==8 && fdDat.matches("\\d+"))
      {
    	  _fldName.add(subFld);
      }else{
          System.out.println("Folder doesnt have the date value");
      }
    }catch(Exception s){
    	s.printStackTrace();}
   }
  }



String _latestFlderPath = _perfOb.dayCalculation(_fldName);
if(!_latestFlderPath.equals("")){
File _subFolder = new File(_latestFlderPath);
File[] _sbFld = _subFolder.listFiles();
for(File _sbFl : _sbFld)
{
	 if(_sbFl.getName().endsWith(".jtl")){
		 _jtlFileFlt.add(_sbFl);
	 }
 }
}else{
	System.out.println("The method returns empty string");
}



Set<String> _jtlFileNames = _perfOb.flterJtlFiles(_jtlFileFlt);
for(String  _flName : _jtlFileNames)
{
 if(_flName.endsWith(".jtl") && _flName.contains("_"))
 {
  try{   
	  
	 System.out.println("<======== Execution begins ==========>");
	 String jtlName = _flName.substring(_flName.lastIndexOf("\\")+1).replace(".jtl", "");
	_brandName  = jtlName.substring(0,jtlName.indexOf("_"));
	_filDate =  jtlName.substring(jtlName.lastIndexOf("_")+1);
	reqNameAndResTm = new ArrayList<String>();
	_uniqueReqName = new LinkedHashSet<String>();
	try{
	  reader1 = new CSVReader(new FileReader(_flName));
    try{   
	    while((data=reader1.readNext())!=null){
	    try{
		   reqNameAndResTm.add(data[2]+","+data[1]);
		   _uniqueReqName.add(data[2]);
		  }catch(Exception e){
		    	System.out.println("The exception occured while reading the data form JTL file:"+e);
		   }
	     }
	    }catch (IOException e){
		       e.printStackTrace();}
	  }catch (FileNotFoundException e){
		       e.printStackTrace();}
	 }catch(Exception e){
	    	  System.out.println("The file name doesnt have the underscore :"+_flName);
	  }

	  
/**This block used to get the unique request with response time.
 * It will return the List
 */
try{
  Iterator<String> itVal = _uniqueReqName.iterator();
  while(itVal.hasNext())
  {  
   String samplerName = itVal.next();
   resTmList = new ArrayList<Integer>();
   for(int i=0; i < reqNameAndResTm.size(); i++)
   {  
		   Pattern pat=Pattern.compile(samplerName+",(.+)");
		   Matcher mat=pat.matcher(reqNameAndResTm.get(i));
		 while(mat.find()){ 
		   if(mat.group(1).trim().matches("\\d+"))
			   resTmList.add(Integer.parseInt(mat.group(1).trim()));
		 }
	}
	if(resTmList.size() > 0)
	      unqVal.put(samplerName.replace("\"", ""), resTmList);
   }
}catch(NullPointerException n){
		 System.out.println("Null pointer execption n: "+n);
}



if(unqVal.size()!=0)
{ 
 Set<String> _keyVal = unqVal.keySet();
 filVal = new ArrayList<String[]>();
 for(String key:_keyVal)
 {
   String[] arr = new String[3];
   List<Integer> intVal = unqVal.get(key);
   Collections.sort(intVal);
 try{
		   arr[0] = key;
		   //System.out.println("The indexval: "+0.9*intVal.size()+"\t"+key+"\t"+intVal.size());  
		   int _index = (int) Math.round(0.9*intVal.size()-1);
		  // System.out.println("The indexval after rounding: "+_index); 
		   arr[1] = intVal.get(_index).toString();
		   if(key.toLowerCase().contains("search")){
			 if(Integer.parseInt(arr[1].trim())>=1500){
				 arr[2]="1500";
				 filVal.add(arr);
			     }
		   }
		 else
		 if(key.toLowerCase().contains("book")){
			 if(Integer.parseInt(arr[1].trim())>=5000){
				 arr[2]="5000";
				 filVal.add(arr);
			 }
		 }else{
			 if(Integer.parseInt(arr[1].trim())>=1500){
			    arr[2]="1500"; 
			    filVal.add(arr);
			 }
		  }
	 }catch(Exception e){
	    	e.printStackTrace();
	    }
	}
}


	/**The following block used to get the error count from csv file
	 * In this block we will get the Map objcet which will be holding the 
	 * req Name and error count value
	 */
	if(unqVal.size()!=0)
	{      int _lIndex = 0,_eIndex = 0;
	       Set<String> keyVal=unqVal.keySet();
	       errVal=new ArrayList<String[]>();
	       CSVReader reader;
	       String errCause;//StringBuilder build=new StringBuilder();
	       Set<String> errDetail;
	try {
		  reader = new CSVReader(new FileReader(_flName.substring(0,_flName.lastIndexOf("\\"))+"/"+_brandName+"_errors_"+_filDate+".csv"));
		  List<String[]> errorList = reader.readAll();
	for(String key:keyVal){
		   int incr=0; boolean flag=false;errCause="";
		   errDetail=new LinkedHashSet<String>();
		   String[] arr=new String[3];
		   List<Integer> intVal=unqVal.get(key);
	 try{
		 
		  arr[0]=key;
		  int totSampler = intVal.size();
		  //System.out.println("The sampler name:"+key+"\t"+totSampler);
		  String[] labelNames = errorList.get(0);
		
		  
	    if(Arrays.toString(labelNames).contains("label") && Arrays.toString(labelNames).contains("failureMessage"))  
	    {
		   for(int l=0;l<labelNames.length;l++){
			  if(labelNames[l].trim().equalsIgnoreCase("label")){
				  _lIndex = l;
			  }
			  if(labelNames[l].trim().equalsIgnoreCase("failuremessage")){
				  _eIndex = l;
			  }
		   }
		   
		   for(int e=1; e<errorList.size(); e++){
			 String[] errorVal = errorList.get(e);
			 if(key.trim().equals(errorVal[_lIndex].trim())){   
			     flag=true;
				 incr++;
				 errDetail.add(errorVal[_eIndex].trim());
			 }
		  }
		 
		  if(flag)
		  {
		     double errPer = (double) (incr*100)/totSampler;
		     DecimalFormat _dFormat = new DecimalFormat("#.00");
		     String _fmtVal = _dFormat.format(errPer);
		    // System.out.println("The request name and size:"+key+"\t"+errDetail.size()+"\t"+incr);
		    if(errPer>=2.0)
		    {    arr[1]=_fmtVal;
		         Iterator<String> errRep=errDetail.iterator();
		        while(errRep.hasNext())
		        {  if(errDetail.size()==1)
		               errCause=errRep.next();  
		              // build.append(errRep.next()); 
			      else
			    	 errCause=errCause+errRep.next()+"<br>"; 
			    }
		        arr[2]=errCause;
		        //System.out.println("The error rate :"+_fmtVal);
		        errVal.add(arr);
		    }
		  }
		  
	     }else{
	    	 System.out.println("The error file doesnt have lable names hence error report can't be genearated");
	         }
	    }catch(Exception e){
	    	  e.printStackTrace();
	      }
	  }
	} catch (FileNotFoundException e1) {
		e1.printStackTrace();
	   }catch (IOException e1) {
			e1.printStackTrace();
		}
	}


	try{
	   if(filVal.size()!=0){
		  List<String[]> srtdList = _perfOb.sortingTheList(filVal);
		  //filMap.put(flName.getName(),srtdList) ;
		  filMap.put(_brandName,srtdList) ;
		  //unqVal.clear();
	    }
	   
	   if(errVal.size()!=0){
		  List<String[]> srtdList=_perfOb.sortingTheList(errVal);
		  //errorMap.put(flName.getName(),srtdList) ;
		  errorMap.put(_brandName,srtdList);
	   }
	  }catch(NullPointerException n){
		  n.printStackTrace();
	 }
	     unqVal.clear();

	}  //End of main if condition
	}  // End of main for loop
	   String _sysDate = _perfOb.getCurrentDate();
	  _perfOb.deleteFileFromFolder(resultFolderPath+"ResultFolder/");
	  _perfOb.perfErrorReport(resultFolderPath+"ResultFolder/"+"PerformanceErrorReport"+_sysDate+".html", errorMap, _brandName);
	  _perfOb.slaBreachedReport(resultFolderPath+"ResultFolder/"+"SLABreached"+_sysDate+".html",filMap,_brandName);
	  
	  System.out.println("<======= Successfully files are created plz check the result folder =======>");
 }else{
	System.out.println("The folder pathe which yor passing doesnt exist in your local machine");
  }
}
}
	


