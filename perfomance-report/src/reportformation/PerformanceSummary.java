package reportformation;
import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import au.com.bytecode.opencsv.CSVReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PerformanceSummary {

	/**The method to generate the sla brached report
	 * It returns the void
	 * and it accepts three parameters
	 * @param path
	 * @param fiVal
	 * @param brandName
	 */	
public void slaBreachedReport(String path,Map<String,String> fiVal,String brandName)
{
 try{ 
    FileWriter writer=new FileWriter(new File(path));
    writer.write("<html><head></head><body style=\"margin: 0 auto;\"><div style=\"display: block; height: 10%; position: fixed; width: 100%; top: 0px; background: none repeat scroll 0% 0% rgb(194, 194, 195);\"><h1 align=\"center\"><u>Perfromance Summary Report</u></h1></div><div align=\"center\" style=\"margin:0,auto;margin-top: 100px;\">");
	//writer.write("<div style=\"height:300px; overflow:auto;\">");
	Iterator<String> filIterator=fiVal.keySet().iterator();
	writer.write("<table border=\"3\" width=\"900\">");
	writer.write("<tr><th width=\"200\">Brands</th><th width=\"420\">Response Time At Ninty Percentage Line</th><th width=\"160\">Average Response TIme</th><th width=\"120\">Average Error TIme</th></tr>");
	while(filIterator.hasNext())
	{   
        String _keyName = filIterator.next();
        String retVal=fiVal.get(_keyName);
		String[] arr=retVal.split("\\|\\|");
	    writer.write("<tr><td>"+_keyName+"</td><td >"+arr[0]+"</td><td >"+arr[1]+"</td><td>"+arr[2]+"</td></tr>");
	}
	  writer.write("</table></div></body></html>");
	  writer.close();
 }catch (IOException e){
	e.printStackTrace();}
  catch(Exception f){
	  f.printStackTrace();}
}

/**The method used to return the current date
 * It returns the string
 * @return
 */
public String getCurrentDate(){
	String curDate = "";
 try{
	Date dt=new Date();
	SimpleDateFormat fmt=new SimpleDateFormat("yyyyMMdd-hhmmss");
	curDate=fmt.format(dt);
   }catch(Exception e){
	   System.out.println("Execption in while returning the current date:"+e);
   }
	return curDate;
}

/**The method used to delete the HTML files from given folder
 * It accepts one parameter as parent folder path
 */
public void deleteFileFromFolder(String path)
{
	try{
	   File resFolder = new File(path);
	   String[] flIndex = resFolder.list();
	   for(String _flIndex : flIndex){
		 if(_flIndex.endsWith(".html")){
			 File fin=new File(resFolder.getPath(), _flIndex);
			 fin.delete();
			 System.out.println("<---- Old html reports are deleted --->");
		 }
	   }
	}catch(Exception e){
		System.out.println("Exception occured while deleting the file from folder: "+e);
	}
}

/**
 * The main method begins here
 * It accepts the two arguments from command line
 * @param args
 */	
public static void main(String... args){
	  

String _resultFolderPath, _jtlFolderPath,_brandName,_filDate = null;
List<String> _reqNameAndResTm = null;
List<Integer> resTmList = null;
List<String[]> _reqNameWithVal = null;
String[] line;

//_jtlFolderPath="D:/apache-jmeter-2.13/bin/";
_jtlFolderPath = args[0];
//_resultFolderPath="C:/Users/ananthreddy.s/Desktop/";_brandName = "";
_resultFolderPath = args[1]; _brandName = "";

Map<String,String>  filMap = new LinkedHashMap<String,String>();
Map<String,List<Integer>> unqVal = new LinkedHashMap<String,List<Integer>>();
Set<String> _uniqueReqName = null;CSVReader _reader1;


PerformanceSummary _perfOb = new PerformanceSummary();

File readFolder = new File(_jtlFolderPath+"IndividualReport/");
File[] lstOfJtlFile = readFolder.listFiles();
for(File _jtFlName : lstOfJtlFile)
{
 if(_jtFlName.getName().endsWith(".jtl"))
 {   
  try{
	  System.out.println("<========Execution begins==========>");
	 _brandName  = _jtFlName.getName().substring(0,_jtFlName.getName().indexOf("_"));
	 _filDate =  _jtFlName.getName().substring(_jtFlName.getName().lastIndexOf("_")+1);
	 _filDate = _filDate.replace(".jtl", "");
	  _reqNameAndResTm = new ArrayList<String>();
	  _uniqueReqName = new LinkedHashSet<String>();
    try{
	    _reader1 = new CSVReader(new FileReader(_jtFlName));
	 try {
		 while((line=_reader1.readNext())!=null){
			 _reqNameAndResTm.add(line[2]+","+line[1]);	 
			 _uniqueReqName.add(line[2]);	 
			}
		}catch (IOException e){
		   System.out.println("the IO exception while reading the line: "+ e);}
	   }catch (FileNotFoundException e) {
		   System.out.println("File not found in the given path: "+_jtFlName +"\n"+e);}
      }catch(Exception e){
    	  System.out.println("The file name doesnt have the underscore: "+_jtFlName);
      }
    

  
/**This block used to get the unique request with response time.
 * It will return the List
 */
try{ 
 Iterator<String> itVal = _uniqueReqName.iterator();
 while(itVal.hasNext())
{  
  String _samplerName = itVal.next();
  resTmList = new ArrayList<Integer>();
  for(int i=0; i < _reqNameAndResTm.size(); i++)
  {  
  	Pattern pat=Pattern.compile(_samplerName+",(.+)");
  	Matcher mat=pat.matcher(_reqNameAndResTm.get(i));
  	while(mat.find()){ 
  	   if(mat.group(1).trim().matches("\\d+"))
  		   resTmList.add(Integer.parseInt(mat.group(1).trim()));
  	 }
  }
   if(resTmList.size() > 0)
      unqVal.put(_samplerName.replace("\"", ""), resTmList);
 } 
}catch(Exception e){
	System.out.println("Null poitner error: "+e);
}


/**The following block used to get the error count from csv file
 * In this block we will get the Map objcet which will be holding the 
 * req Name and error count value
 */
if(unqVal.size()!=0)
{      int _index = 0,_sum;Integer _avg; CSVReader reader;
       Set<String> _keyVal = unqVal.keySet();
       _reqNameWithVal = new ArrayList<String[]>();
try{
       reader = new CSVReader(new FileReader(_jtlFolderPath+"Performance/"+_brandName+"_errors_"+_filDate+".csv"));
       List<String[]> errorList = reader.readAll();
  for(String key:_keyVal)
  {     _sum = 0;int incr=0; boolean flag=false;
       String[] arr = new String[4];
       List<Integer> intVal = unqVal.get(key);
       int totSampler = intVal.size();
       Collections.sort(intVal);
       
    try
   {    arr[0] = key;
	    for(int i=0; i < totSampler; i++){
		 _sum = _sum + intVal.get(i);
	    }
	    _avg = (_sum / intVal.size());
	   _index = (int) Math.round(0.9*intVal.size()-1);
	    arr[1] = intVal.get(_index).toString();
	    arr[2] = _avg.toString();
	}
    catch(Exception e){
    	e.printStackTrace();
    }
    
   for(int e=1; e < errorList.size(); e++)
   {
	   String[] errorVal = errorList.get(e);
	   if(key.trim().equals(errorVal[0].trim()))
	   {   
	     flag=true;
		 incr++;
	   }
   }
   if(flag)
   {
       double errPer = (double) (incr*100)/totSampler;
       DecimalFormat _dFormat = new DecimalFormat("#.00");
       String _fmtVal = _dFormat.format(errPer);
       arr[3]=_fmtVal;
     }else{
    	arr[3] = "0.00" ;
     }
     _reqNameWithVal.add(arr);
  }
}catch(FileNotFoundException f){
	  System.out.println("The file not found in the given path:"+_jtFlName+"\n"+f);
}catch(Exception e){
	  System.out.println("Exception occure while reading the file:"+_jtFlName+"\n"+e);
}
}


try{
    if(_reqNameWithVal.size()!=0)
    {
	      Float avgRes,avgNinty,avgError;
	      Integer sumN = 0,sumR=0;float sumE=0;
	  for(int i=0; i < _reqNameWithVal.size(); i++){
		   String[] str = _reqNameWithVal.get(i);
		 try{
			 sumN = sumN + Integer.parseInt(str[1]);
			 sumR = sumR + Integer.parseInt(str[2]);
			 sumE = sumE + Float.parseFloat(str[3]);
		 }catch(Exception e){
			 System.out.println("Exception caused While parsing the string to float: "+e);
		 }
	  }
	  System.out.println("The average of Res and error and Ninty percentage line: "+(float)(sumN/_reqNameWithVal.size())/1000+"\t"+(float)(sumR/_reqNameWithVal.size())/1000+"\t"+(sumE/_reqNameWithVal.size()));
	  avgRes = (float) (sumR/_reqNameWithVal.size()) / 1000;
	  avgNinty = (float)(sumN/_reqNameWithVal.size()) / 1000;
	  avgError = (float) sumE / _reqNameWithVal.size();
	  
	  String reqName = _jtFlName.getName().replace(".jtl", "").replaceAll("\\d+","");
	  filMap.put(reqName,avgNinty.toString()+" sec"+"||"+avgRes.toString()+" sec"+"||"+avgError.toString()+"%") ;
	  unqVal.clear();
  }
}catch(NullPointerException n){
	n.printStackTrace();
}
  
}     //End of main if condition
}    //End of main for loop 

      String _sysDate = _perfOb.getCurrentDate();
     _perfOb.slaBreachedReport(_resultFolderPath+"ResultFolder/"+"SummaryReport"+_sysDate+".html", filMap, _brandName);
     System.out.println("<=======Successfully Summary Reports are created plz check the result folder=======>");
     
}

}


